#Creando la base de datos zapateria
CREATE DATABASE bdzapateria CHARSET = utf8mb4 COLLATE = UTF8MB4_UNICODE_CI;

USE bdzapateria;

#creando la tabla clientes
CREATE TABLE clientes (
	#creando llave primaria de la tabla clientes y sus campos
	id_cliente INT AUTO_INCREMENT PRIMARY KEY,
	dni INT(8),
	nombre VARCHAR(50) NOT NULL,
	pais VARCHAR(50)
) ENGINE = InnoDB DEFAULT CHARSET = utf8mb4 COLLATE = UTF8MB4_UNICODE_CI;

#creando la tabla productos
CREATE TABLE productos (
	#creando llave primaria de la tabla clienteproductos y sus campos
	id_producto INT AUTO_INCREMENT PRIMARY KEY,
	nombre VARCHAR(100) NOT NULL,
	presentacion VARCHAR(100),
	valor DECIMAL(10, 2)
) ENGINE = InnoDB DEFAULT CHARSET = utf8mb4 COLLATE = UTF8MB4_UNICODE_CI;

#creando la tabla inventario que estara relacionado con productos
CREATE TABLE inventario(
	#creando llave primaria de la tabla inventario y sus campos
	id_inventario INT AUTO_INCREMENT PRIMARY KEY,
	fk_producto INT NOT NULL,
	tipo_movimiento BOOLEAN,
	fecha DATE,
	cantidad INT(9),
	FOREIGN KEY(fk_producto) REFERENCES productos(id_producto)
) ENGINE = InnoDB DEFAULT CHARSET = utf8mb4 COLLATE = UTF8MB4_UNICODE_CI;

#creamos la tabla facturacion que relacionara  clientes con sus productos comprados
CREATE TABLE facturacion(
	#creando llave primaria de la tabla facturacion y sus campos
	id_facturacion INT AUTO_INCREMENT PRIMARY KEY,
	fk_cliente INT NOT NULL,
	fk_producto INT NOT NULL,
	impuesto DECIMAL(10, 2),
	descuento DECIMAL(10, 2),
	valor_pagar DECIMAL(10, 2),
	fecha DATE,
	FOREIGN KEY(fk_cliente) REFERENCES clientes(id_cliente),
	FOREIGN KEY(fk_producto) REFERENCES productos(id_producto)
) ENGINE = InnoDB DEFAULT CHARSET = utf8mb4 COLLATE = UTF8MB4_UNICODE_CI;