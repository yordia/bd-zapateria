#CONSULTA 01
SELECT
    SUM(valor_pagar)
FROM
    facturacion
WHERE
    fk_cliente = 2;

#CONSULTA 02
SELECT
    SUM(valor_pagar)
FROM
    facturacion
WHERE
    fk_producto = 1;

#CONSULTA 03
SELECT
    *
FROM
    facturacion
WHERE
    fecha >= '2020-06-02'
    AND fecha <= '2020-06-03';

#CONSULTA 04
#Clientes que han comprando por lo menos 1 vez
#Usamos distinct para que no se repitan los clientes
#Hacemos un inner join para obtener los datos del cliente como nombre y dni
SELECT
    DISTINCT nombre,
    dni
FROM
    facturacion t1
    INNER JOIN clientes t2 ON t1.fk_cliente = t2.id_cliente